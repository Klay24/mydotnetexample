﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using static DAL.DataContent.DatabaseContext;

namespace DAL.DataContent
{
    public class DatabaseContext : DbContext
    {
        public class OptionsBuilder
        {
            public OptionsBuilder ()
            {
                settings = new AppConfiguration();
                opsBuider = new DbContextOptionsBuilder<DatabaseContext>();
                opsBuider.UseSqlServer(settings.sqlConnectionString);
                dbOptions = opsBuider.Options;
            }
            public DbContextOptionsBuilder<DatabaseContext> opsBuider
            {
                get; set;
            }
            public DbContextOptions<DatabaseContext> dbOptions
            {
                get; set;
            }
            private AppConfiguration settings
            {
                get; set;
            }

        }
        public static OptionsBuilder ops = new OptionsBuilder();

        public DatabaseContext (DbContextOptions<DatabaseContext> options) : base(options) { }

        public DbSet<User> User
        {
            get;set;
        }
    }
}
