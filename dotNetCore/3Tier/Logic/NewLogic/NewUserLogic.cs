﻿using Api.Interface;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Logic.NewLogic
{
    public class NewUserLogic : INewUserLogic
    {
        private IRepository<User> _user;
        public NewUserLogic(IRepository<User> user)
        {
            _user = user;
        }

        public async Task<Boolean> CreateNewUser (string userName, string userPassword, string email, int authLevel)
        {
            try
            {
                var newUser = new User { Username = userName, Password = userPassword, Email = email, AuthLevelRefId = authLevel };
                await _user.Add(newUser);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IEnumerable<User>> GetAllUser ()
        {
            IEnumerable<User> users = await _user.GetAll();
            return users;
        }

        public async Task<User> GetById (int id)
        {
            User user = await _user.GetById(id);
            return user;
        }

        public async Task Update (int id, string userName, string userPassword, string email, int authLevel)
        {
            var newUser = new User { Id = id, Username = userName, Password = userPassword, Email = email, AuthLevelRefId = authLevel };
            await _user.Update(newUser);
        }

        public async Task DeleteById (int id)
        {
            var user = await GetById(id);
            await _user.Remove(user);
        }
    }
}
