﻿using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Logic.UserLogic
{
    public class UserLogic
    {
        private IUser _user = new DAL.Functions.UserFunctions();

        public async Task<Boolean> CreateNewUser (string username, string emailAddress, string password, int authLevelId)
        {
            try
            {
                var result = await _user.AddUser(username, emailAddress, password, authLevelId);
                if (result.Id > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<List<User>> GetAllUser ()
        {
            List<User> users = await _user.GetAllUsers();
            return users;
        }

        public async Task<User> GetById (int id)
        {
            User user = await _user.GetById(id);
            return user;
        }

        public async Task UpdateById (int id, string username, string email, string password, int authLevelId)
        {
            await _user.UpdateById(id, username, email, password ,authLevelId);
        }

        public async Task DeleteById (int id)
        {
            await _user.DeleteById(id);
        }
    }
}
