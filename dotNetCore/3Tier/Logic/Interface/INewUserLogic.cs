﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Interface
{
    public interface INewUserLogic
    {
        Task<Boolean> CreateNewUser(string username, string emailAddress, string password, int authLevelId);

        Task<IEnumerable<User>> GetAllUser();

        Task<User> GetById(int id);

        Task Update(int id, string username, string email, string password, int authLevelId);

        Task DeleteById(int id);
    }
}
