﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        Task<T> GetById (int id);

        Task<IEnumerable<T>> GetAll ();

        Task<T> Update (T entity);

        Task<IEnumerable<T>> Find (Expression<Func<T, bool>> predicate);

        Task<T> Add (T entity);

        Task AddRange (IEnumerable<T> entities);

        Task Remove (T entity);

        Task RemoveRange (IEnumerable<T> entities);
    }
}
