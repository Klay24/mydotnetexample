﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IUser
    {
        Task<User> AddUser (string username, string emailAddress, string password, int authLevelId);
        Task<List<User>> GetAllUsers ();
        Task<User> GetById (int id);
        Task UpdateById (int id, string username, string email, string password, int authLevelId);
        Task DeleteById (int id);
    }
}
