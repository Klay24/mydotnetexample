﻿using DAL.DataContent;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Functions
{
    public class UserFunctions : IUser
    {
        public async Task<User> AddUser (string username, string emailAddress, string password, int authLevelId)
        {
            User newUser = new User
            {
                Email = emailAddress,
                Password = password,
                Username = username,
                AuthLevelRefId = authLevelId
            };
            using (var context = new DatabaseContext(DatabaseContext.ops.dbOptions))
            {
                await context.Users.AddAsync(newUser);
                await context.SaveChangesAsync();
            }
            return newUser;
        }

        public async Task<List<User>> GetAllUsers ()
        {
            List<User> users = new List<User>();
            using (var context = new DatabaseContext(DatabaseContext.ops.dbOptions))
            {
                users = await context.Users.ToListAsync();
            }
            return users;
        }

        public async Task<User> GetById (int id)
        {
            User user;
            using (var context = new DatabaseContext(DatabaseContext.ops.dbOptions))
            {
                user = await context.Users.FirstOrDefaultAsync(x => x.Id == id);
            }
            return user;
        }

        public async Task DeleteById (int id)
        {
              using (var context = new DatabaseContext(DatabaseContext.ops.dbOptions))
                {
                    var user = GetById(id).Result;
                    context.Users.Remove(user);
                    await context.SaveChangesAsync();
                }
        }

        public async Task UpdateById (int id, string username, string email, string password, int authLevelId)
        {
            User user;
            using (var context = new DatabaseContext(DatabaseContext.ops.dbOptions))
            {
                user = GetById(id).Result;
                user.AuthLevelRefId = authLevelId;
                user.Email = email;
                user.Password = password;
                user.Username = username;
                context.Users.Update(user);
                await context.SaveChangesAsync();
            }
        }
    }
}
