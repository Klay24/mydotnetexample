﻿using DAL.DataContent;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    public class SQLRepository<T> : IRepository<T> where T : class
    {
        private readonly DatabaseContext _context;

        public SQLRepository (DatabaseContext databaseContext)
        {
            //_context = new DatabaseContext(DatabaseContext.ops.dbOptions);
            _context = databaseContext;
        }

        public async Task<T> GetById (int id)
        {
            return await _context.Set<T>().FindAsync(id);
        }

        public async Task<IEnumerable<T>> GetAll ()
        {
            return await _context.Set<T>().ToListAsync();
        }

        public async Task<IEnumerable<T>> Find (Expression<Func<T, bool>> predicate)
        {
            return await _context.Set<T>().Where(predicate).ToListAsync();
        }

        public async Task<T> Add (T entity)
        {
            _context.Set<T>().Add(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task AddRange (IEnumerable<T> entities)
        {
            await _context.Set<T>().AddRangeAsync(entities);
        }

        public async Task Remove (T entity)
        {
            _context.Set<T>().Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task RemoveRange (IEnumerable<T> entities)
        {
            _context.Set<T>().RemoveRange(entities);
            await _context.SaveChangesAsync();
        }

        public async Task<T> Update (T newEntity)
        {
            _context.Set<T>().Update(newEntity);
            await _context.SaveChangesAsync();
            return newEntity;
        }
    }
}
