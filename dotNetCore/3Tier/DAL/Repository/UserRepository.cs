﻿using DAL.DataContent;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DAL.Repository
{
    public class UserRepository : SQLRepository<User>
    {
        public UserRepository(DatabaseContext databaseContext) : base(databaseContext)
        {
        
        }
       //可以在這裡增加一些非所有repository共有的方法
       //例如找出所有男性user
    }
}
