﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Entities
{
    public class Dog
    {
        [Key]
        public int Id
        {
            get; set;
        }
        [Required]
        public string DogName
        {
            get; set;
        }
    }
}
