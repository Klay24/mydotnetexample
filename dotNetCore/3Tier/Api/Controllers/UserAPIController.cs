﻿using Api.Model;
using Api.ViewModel;
using Logic.UserLogic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserAPIController : ControllerBase
    {
        private UserLogic userLogic = new UserLogic();

        [HttpPost]
        public async Task<Boolean> AddUser (User user)
        {
            bool result = await userLogic.CreateNewUser(user.Username, user.Email, user.Password, user.AuthLevel);
            return result;
        }

        [HttpGet]
        public async Task<List<UserViewModel>> GetAllUsers ()
        {
            var userList = new List<UserViewModel>();
            var users = await userLogic.GetAllUser();
            if (users.Count > 0)
            {
                foreach (var user in users)
                {
                    var currentUser = new UserViewModel
                    {
                        AuthLevel = user.AuthLevelRefId,
                        Email = user.Email,
                        UserId = user.Id,
                        Username = user.Username,
                    };
                    userList.Add(currentUser);
                }
            }
            return userList;
        }


        [HttpGet("{id}")]
        public async Task<UserViewModel> GetUserById (int id)
        {
            var rawUser = await userLogic.GetById(id);
            var user = new UserViewModel { UserId = rawUser.Id, Username = rawUser.Username, Email = rawUser.Email, AuthLevel = rawUser.AuthLevelRefId };
            return user;
        }

        [HttpPut("{id}")]
        public async Task<Boolean> UpdateById ([FromRoute] int id, User user)
        {
            try
            {
                await userLogic.UpdateById(id, user.Username, user.Email, user.Password, user.AuthLevel);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpDelete("{id}")]
        public async Task<Boolean> DeleteById (int id)
        {
            try
            {
                await userLogic.DeleteById(id);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
