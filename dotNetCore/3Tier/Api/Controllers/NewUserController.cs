﻿using Api.Interface;
using Api.Model;
using Api.ViewModel;
using Logic.NewLogic;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewUserController : Controller
    {
        private INewUserLogic newUserLogic;

        public NewUserController(INewUserLogic _newUserLogic)
        {
            newUserLogic = _newUserLogic;
        }

        [HttpPost]
        public async Task<Boolean> AddUser (User user)
        {
            bool result = await newUserLogic.CreateNewUser(user.Username, user.Password, user.Email, user.AuthLevel);
            return result;
        }

        [HttpGet]
        public async Task<List<UserViewModel>> GetAllUsers ()
        {
            var userList = new List<UserViewModel>();
            var users = await newUserLogic.GetAllUser();
            if (users.ToList().Count > 0)
            {
                foreach (var user in users)
                {
                    var currentUser = new UserViewModel
                    {
                        AuthLevel = user.AuthLevelRefId,
                        Email = user.Email,
                        UserId = user.Id,
                        Username = user.Username,
                    };
                    userList.Add(currentUser);
                }
            }
            return userList;
        }


        [HttpGet("{id}")]
        public async Task<UserViewModel> GetUserById (int id)
        {
            var rawUser = await newUserLogic.GetById(id);
            var user = new UserViewModel { UserId = rawUser.Id, Username = rawUser.Username, Email = rawUser.Email, AuthLevel = rawUser.AuthLevelRefId };
            return user;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<Boolean> UpdateById ([FromRoute]int id, User user)
        {
            try
            {
                await newUserLogic.Update(id , user.Username, user.Password, user.Email, user.AuthLevel);
                return true;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }

        [HttpDelete("{id}")]
        public async Task<Boolean> DeleteById (int id)
        {
            try
            {
                await newUserLogic.DeleteById(id);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
