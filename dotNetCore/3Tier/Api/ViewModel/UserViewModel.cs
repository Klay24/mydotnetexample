﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModel
{
    public class UserViewModel
    {
        public Int64 UserId
        {
            get; set;
        }
        public string Username
        {
            get; set;
        }
        public string Email
        {
            get; set;
        }
        public Int32 AuthLevel
        {
            get; set;
        }
    }
}
