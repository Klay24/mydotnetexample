﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace testUseApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestUseController : ControllerBase
    {
        // GET: api/<TestUseController>
        [HttpGet]
        [Route("first")]
        public string Get ()
        {
            return "success";
        }

        [HttpGet]
        [Route("second")]
        public int Next ()
        {
            return 100;
        }

        // GET api/<TestUseController>/5
        [HttpGet("{id}")]
        public string Get (int id)
        {
            return "value";
        }

        // POST api/<TestUseController>
        [HttpPost]
        public void Post ([FromBody] string value)
        {
        }

        // PUT api/<TestUseController>/5
        [HttpPut("{id}")]
        public void Put (int id, [FromBody] string value)
        {
        }

        // DELETE api/<TestUseController>/5
        [HttpDelete("{id}")]
        public void Delete (int id)
        {
        }
    }
}
